


### View the graph at [ssgraham767.gitlab.io/tree-of-trees/](https://ssgraham767.gitlab.io/tree-of-trees/)

----
<div style="display: flex; justify-content: center;">
    <img src="example.gif" alt="Example Animation" style="width: 400px;">
</div>

*If the gif is frozen, try reloading the page*

---
**Controls:**
* **Click and drag** to **pan** around
* **Scroll** to adjust your **zoom**
* **Click** on an item to go to its **Wikipedia** page

---
## Description:
**This is a project which uses a pair of scripts to pull data about each item in [Wikipedia's *List of Trees and Shrubs*](https://en.wikipedia.org/wiki/List_of_trees_and_shrubs_by_taxonomic_family) and generate a graph of the listed species, showing how they are related to each other taxonomically.**

*This list is by no means exhaustive. As Wikipedia usership is highest in North America and Europe, this list is likely to be better representative of species seen in these areas.*
Color indicates depth on the taxonomic tree—the distance from the root node for all vascular plants (the "Tracheophyte" clade).
Larger (and more opaque) nodes indicate greater levels of centrality (in this case, calculated as the percentage of edges which pass through a node), which allows us to see which clades account for greatest biodiversity.

---
## Setup
* To set up a conda environment, including all dependencies, run `conda env create -f environment.yml`
* To use the conda environment, run `conda activate tree-of-trees-env` in your terminal to activate the environment prior to running any other scripts

## Fetching Data:  
To pull fresh data, run `scrape_clades.py` using `python scrape_clades.py`
    ```
    cd /Users/user/Desktop/tree-of-trees && python scrape_clades.py
    ```
* Note: This should really use [a dumped wikipedia database](https://en.wikipedia.org/wiki/Wikipedia:Database_download) instead. As is, it has a slow crawl rate and will take quite a while, though it does allow us to present html in a style matching that of the screen.
* There are normally a couple urls for which wikipedia doesnt have the full taxonomy. These will throw 'nx.NetworkXNoPath' errors when running `make_graph.py`, so they should be easy to find. Normally, they are next to other species of the same genus in the csv, so it is normally easy to fill in data for them.

## Generating the Graph:
2. To make alterations to the graph and webpage, edit `make_graph.py` and `html_template.html`, respectively.
3. To regenerate the graph locally during development execute `make_graph.py` using `python make_graph.py`.
    ```
    cd /Users/user/Desktop/tree-of-trees && python make_graph.py
    ```
4. Then, you can view the output by opening index.html in your browser.
5. When changes are ready, a merge to main will trigger [a pipeline](https://gitlab.com/ssgraham767/tree-of-trees/-/pipelines) in which [index.html](https://gitlab.com/ssgraham767/tree-of-trees/-/blob/9b823c2e0edc8d50f7c2c495cd888383005a2974/index.html) will be regenerated with the latest version of [the template](https://gitlab.com/ssgraham767/tree-of-trees/-/blob/9b823c2e0edc8d50f7c2c495cd888383005a2974/html_template.html), and the site will be redeployed.