import networkx as nx
import pandas as pd
from networkx.drawing.nx_agraph import graphviz_layout
import ast
import numpy as np
from bokeh.plotting import figure, output_file, save, from_networkx
from bokeh.models import ColumnDataSource, HoverTool, OpenURL, TapTool
from bokeh.transform import linear_cmap
from bokeh.palettes import Viridis7
from bokeh.embed import components


def import_data(path):
    data = pd.read_csv(path)
    data['taxonomy'] = data['taxonomy'].fillna('{}').apply(ast.literal_eval)
    data['nearest_taxon'] = data.apply(lambda row: row['taxonomy'][-1] if row['taxonomy'] else None, axis=1)
    data['image_html'] = data['image_html'].fillna('')
    data = data.drop_duplicates('name')
    return data

def transform_to_graph(data):
    G = nx.Graph()
    G.add_nodes_from(data['name'])
    data.apply(lambda row: G.add_edge(row['name'], row['nearest_taxon']) if row['nearest_taxon'] else None, axis=1)
    for taxonomy in data['taxonomy']:
        if len(taxonomy)>1:
            for i, taxon in enumerate(taxonomy):
                if i != 0:
                    G.add_edge(taxonomy[i], taxonomy[i-1])
    return G

def calculate_distance_from_root_node(G):
    shortest_paths = [nx.shortest_path_length(G, 'Tracheophytes', n) for n in G.nodes()]
    max_shortest_path = max(shortest_paths)
    distance_from_root_node = np.array([(max_shortest_path - path) for path in shortest_paths])
    return distance_from_root_node

def make_graph_params(G, distance_from_root_node, image_html):
    centrality = np.array(list(nx.betweenness_centrality(G).values()))
    pos = graphviz_layout(G, prog='sfdp')
    coords = np.array(list(pos.values()))
    x = coords[:,0]
    y = coords[:,1]
    node_sizes = 8 + centrality*150 # set a minimum of 8
    average_node_size = np.mean(node_sizes)

    source = ColumnDataSource(dict(
        x=x , y=y , 
        sizes=node_sizes, hover_sizes = node_sizes*1.5, 
        labels=list(G.nodes()), 
        colors=distance_from_root_node, 
        alpha= .3 + (centrality/max(centrality)*.6),
        image_html = image_html
        ))

    return {
        'G': G,
        'pos': pos, 
        'x': coords[:,0],
        'y': coords[:,1],
        'x_range': (min(x)-10*average_node_size, max(x)+10*average_node_size),
        'y_range': (min(y)-10*average_node_size, max(y)+10*average_node_size),
        'node_transparencies': .3 + (centrality/max(centrality)*.6), # set a minimum of .3,
        'color_mapper': linear_cmap(field_name='colors', palette=Viridis7, low=min(distance_from_root_node), high=max(distance_from_root_node)),
        'color_complement_mapper': linear_cmap(field_name='colors', palette=Viridis7, low=max(distance_from_root_node), high=min(distance_from_root_node)),
        'source': source
        }

def generate_plot(output_path, graph_params):
    # create a Bokeh plot
    plot = figure(
        # title="Taxonomic Tree of Trees - Based on Wikipedia's List of Trees and Shrubs", 
        x_range = graph_params['x_range'], 
        y_range = graph_params['y_range'], 
        tools=['wheel_zoom, pan, tap'], active_scroll="wheel_zoom", 
        toolbar_location='above',
        width=1380, height=750,
        background_fill_color = "black",
        background_fill_alpha = 0.33,
        match_aspect = True,
        aspect_scale = .7
        )

    plot.title.text_font_size = '16pt'

    # convert the graph to a Bokeh data source
    graph_renderer = from_networkx(graph_params['G'], graph_params['pos'], scale=2, center=(np.mean(graph_params['x']), np.mean(graph_params['y'])), node_labels=list(graph_params['G'].nodes()))
    graph_renderer.edge_renderer.glyph.update(line_width=2, line_alpha=0.25, line_color = 'purple')
    plot.renderers.append(graph_renderer)

    # Plot the circles to show when not activated by hovering
    plot.circle('x', 'y', size='sizes',
                    fill_color=graph_params['color_mapper'], 
                    hover_fill_color=graph_params['color_mapper'],
                    fill_alpha='alpha', hover_alpha=0.0,
                    line_color=graph_params['color_complement_mapper'], 
                    hover_line_color=graph_params['color_mapper'],
                    line_alpha=.8, hover_line_alpha=0,
                    line_width=.5, hover_line_width=0,  
                    source=graph_params['source'])

    # Plot the circles to show when activated by hovering
    cr = plot.circle('x', 'y', size='hover_sizes',
                    fill_color=graph_params['color_mapper'], 
                    hover_fill_color=graph_params['color_mapper'],
                    fill_alpha=0.0, hover_alpha=0.6,
                    line_color=graph_params['color_complement_mapper'], 
                    hover_line_color=graph_params['color_mapper'],
                    line_alpha=0.0, hover_line_alpha=1,
                    line_width=0, hover_line_width=1.5, 
                    source=graph_params['source'])

    TOOLTIPS = f"""
        <div style="background-color: black;">
            <span style="font-size: 12px; font-weight: bold; display: block; text-align: center; color: white;">@labels</span>
            @image_html
        </div>
        """

    plot.add_tools(HoverTool(tooltips=TOOLTIPS, renderers=[cr]))

    # open the wiki page on click
    url = "https://www.wikipedia.org/wiki/@labels"
    taptool = plot.select(type=TapTool)
    taptool.callback = OpenURL(url=url)

    plot.axis.visible = False
    plot.grid.visible = False

    # export the plot to a script and div
    script, div = components(plot)

    # read the HTML file
    with open('html_template.html', 'r') as file:
        html = file.read()

    # replace the placeholder with the script and div
    html = html.replace('<div id="plot-div-placeholder"></div>', div)
    html = html.replace('<div id="plot-script-placeholder"></div>', script)

    # write the result back to the HTML file
    with open(output_path, 'w') as file:
        file.write(html)
    
    print(f'Graph Visualization Generated at {output_path}')
    

def main(input_path = 'trees_and_shrubs.csv', output_path='tree_of_trees.html'):
    data = import_data(input_path)
    G = transform_to_graph(data)
    distance_from_root_node = calculate_distance_from_root_node(G)
    graph_params = make_graph_params(G, distance_from_root_node, data['image_html'])
    generate_plot(output_path, graph_params)

if __name__ == "__main__":
    main(input_path = 'trees_and_shrubs.csv', output_path='index.html')