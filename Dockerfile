# seansgraham/simple-sites:1.0.0
FROM ruby:2.7

# Install necessary packages for pip and Graphviz
RUN apt-get update -y && apt-get install -y python3-pip graphviz libgraphviz-dev

# Install Python packages
RUN pip3 install pandas==2.0.3 numpy==1.24 networkx==3.1 bokeh==2.4.3 pygraphviz==1.11

# Create a symbolic link for python
RUN ln -s /usr/bin/python3 /usr/bin/python

# docker build -t seansgraham/simple-sites:1.0.0 .