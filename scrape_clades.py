import requests
from bs4 import BeautifulSoup
import csv
from urllib.parse import urljoin
import re
from time import sleep
import os
from datetime import datetime
import statistics


# The url of wikipedia's list of trees and shrubs
URL = os.getenv('URL')


# Functions
def get_html_feature(url, tag, attrs):
    "Get all features from the page which match the given tag and attrs"
    response = requests.get(url, timeout=35)
    page_text = BeautifulSoup(response.text, features="html.parser")
    page_features = page_text.find_all(tag, attrs=attrs)
    return page_features


def check_if_species_name(string: str):
    "Determine is a string is a name in binomial nomenclature format"
    pattern = r'[A-Z][a-z]{2,} [a-z]{2,}'
    if re.search(pattern, string):
        return True
    else:
        return False
    

def get_page_links(url):
    "Get links from the page which match binomial nomenclature pattern"
    tree_and_shrub_links = []
    # fetch the page
    response = requests.get(url)
    soup = BeautifulSoup(response.content, 'html.parser')    

    # loop through all the links and find those with patterns matching species names
    links = soup.find_all('a')
    for link in links:
        if link.has_attr('title') and check_if_species_name(link['title']) and len(link['title'].split()) <= 3 and '(' not in link['title'] and ':' not in link['title'] and '=' not in link['title']:
            if link['title'] == 'Analog forestry':
                break
            absolute_url = urljoin(URL, link.get('href'))
            tree_and_shrub_links.append(absolute_url)
    print(f'skipping links: {tree_and_shrub_links[:6]}')
    return tree_and_shrub_links[6:] # removing a few extras - this may need updated if run again


def get_properties_and_ancestors(url):
    """Given the url of a species, pull some basic properties such as its name along with a list of the clades of which it is a member"""
    page_properties, ancestor_links = {}, {}

    page_html = get_html(url)
    if not page_html:
        return page_properties, ancestor_links

    page_properties['name'] = page_html.find('title').text[:-12]

    # fetch the biological data infobox and get taxonomic and image data contained within
    infobox_html = get_infobox(page_html)
    if not infobox_html:
        return page_properties, ancestor_links

    page_properties['image_html'] = get_image_html(infobox_html)
    infobox_tables = infobox_html.find_all('td', attrs=None)
    page_properties['taxonomy'] = []
    
    for i, _ in enumerate(infobox_tables):
        for level in ['Genus', 'Tribe', 'Family', 'Superfamily', 'Order', 'Class', 'Division', 'Clade']:
            if level in str(infobox_tables[i]):
                try:
                    ancestor_link = infobox_tables[i+1].find('a')
                    ancestor_name = [i.text.replace('\n', '') for i in infobox_tables[i+1].children][0]
                    if ancestor_name not in page_properties['taxonomy'] and 'Kingdom' not in ancestor_name: 
                        page_properties['taxonomy'].append(ancestor_name)
                        ancestor_links[ancestor_name] = ancestor_link.get('href')
                except:
                    # since this is only called on species, we can get the genus from the name
                    genus = page_properties['name'].split()[0]
                    page_properties['taxonomy'].append(genus)
                    pass

    return page_properties, ancestor_links


def get_html(url):
    """Fetch the text from a given url"""
    try:
        response = requests.get(url, timeout=35)
    except Exception as e:
        print(f'Retrying url ({url}) due to error: {e}')
        try:
            sleep(25)
            response = requests.get(url, timeout=35)
        except Exception as e:
            print(f'Unable to reach url ({url}): {e}')
            return

    return BeautifulSoup(response.text, features="html.parser")


def get_infobox(page_html):
    """Given the html of a page, fetch the html code used to render its infobox on wikipedia"""
    infobox_html = page_html.find('table', attrs={'class': 'infobox biota'})
    if not infobox_html:
        print('No infobox biota found')
        return None

    return infobox_html


def get_image_html(infobox_html):
    """Given the html of an infobox for a page, fetch the html code used to render its profile picture on wikipedia"""
    profile_picture = str(infobox_html.find('img', attrs={'class': 'mw-file-element'})).replace('//upload.wikimedia', 'https://upload.wikimedia')

    if not profile_picture:
        print('No image found')
        return None
        
    return profile_picture


def get_species_data(tree_and_shrub_links, writer, crawl_rate):
    total = len(tree_and_shrub_links)
    looptimes = []
    full_ancestor_links = dict()
    last_loop = datetime.now()
    for i, species_url in enumerate(tree_and_shrub_links):

        len_ancestor_links = len(full_ancestor_links)
        print(f"{i+1} of {total}: {species_url} \nNumber of ancestral clades: {len_ancestor_links}")

        page_properties, ancestor_links = get_properties_and_ancestors(species_url)
        if page_properties.get('name'): 
            writer.writerow([page_properties['name'], page_properties.get('taxonomy'), page_properties.get('image_html')])
            full_ancestor_links.update(ancestor_links)

        loop_time = datetime.now() - last_loop
        loop_time_in_seconds = int(loop_time.microseconds)/1000000
        looptimes.append(loop_time_in_seconds)
        average_loop_time = statistics.mean(looptimes)
        last_loop = datetime.now()

        minutes_remaining = round(((total-i) * (crawl_rate+0.05)/60), 3)
        print(f"An estimated {minutes_remaining} minutes remain in part 1")
        
        sleep(crawl_rate)
        
    return full_ancestor_links


def get_taxon_images(full_ancestor_links, writer, crawl_rate):
    looptimes = []
    number_of_links = len(full_ancestor_links)
    last_loop = datetime.now()
    for i, item in enumerate(full_ancestor_links):
        start_time = datetime.now()

        print(f"{i+1} of {number_of_links}: {item} \n")
        if full_ancestor_links.get(item) is not None and len(full_ancestor_links.get(item)) > 0:
            page_html = get_html('https://en.wikipedia.org'+full_ancestor_links[item])
            infobox_html = get_infobox(page_html)
            if infobox_html:
                image_html = get_image_html(infobox_html)
        else:
            image_html = None
        writer.writerow([item, [], image_html])

        loop_time = datetime.now() - last_loop
        loop_time_in_seconds = int(loop_time.microseconds)/1000000
        looptimes.append(loop_time_in_seconds)
        average_loop_time = statistics.mean(looptimes)
        last_loop = datetime.now()

        minutes_remaining = round(((number_of_links-i) * (crawl_rate+average_loop_time)/60), 3)
        print(f"An estimated {minutes_remaining} minutes remain in part 2")
        
        sleep(crawl_rate)


def main(URL, output_path = 'trees_and_shrubs.csv'):
    """
    Given a the url for the wikipedia list of tree species:
    1. Pull the information from each page, including its clade information.
    2. Look up the images for each species and clade of which it is a member.
    3. Write all the information to a csv during this process (incrementally)
    """
    tree_and_shrub_links = get_page_links(URL)
    crawl_rate = 2
    
    # Open the output file for writing
    with open(output_path, 'w', newline='') as csvfile:
        # create a csv writer object
        writer = csv.writer(csvfile)
        writer.writerow(['name', 'taxonomy', 'image_html'])

        print("This process happens in two steps.\nThe first part gets the species data, and the second fetches the images for ancestral clades.\nThe number of ancestral clades is unknown until after the first part runs, but has tended to be about ~600")
        print("Part 1 of 2:\n____________________________")
        full_ancestor_links = get_species_data(tree_and_shrub_links, writer, crawl_rate)
        
        print("Part 2 of 2:\n____________________________")
        get_taxon_images(full_ancestor_links, writer, crawl_rate)
        looptimes = []
        number_of_links = len(full_ancestor_links)
            
    print(f'script complete: data collected at {output_path}')


if __name__ == "__main__":
    main(URL, output_path = 'trees_and_shrubs.csv')